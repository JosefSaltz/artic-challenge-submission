import { useQuery } from "@tanstack/react-query";

export function useBlockDataQuery() {
  return useQuery({ queryKey: ["KnowledgeCheckData"], queryFn: fetchBlockData });
}

async function fetchBlockData() {
  // Fetch via GET request to our endpoint URL
  const request = await fetch("http://localhost:5001/knowledge-check-blocks", {
    method: "GET",
  });
  // Happy Pathing Guard
  if(!request.ok) throw new Error("Something went wrong trying to request knowledge check block data");
  // Return the request body parsed as JSON
  return await request.json();
}