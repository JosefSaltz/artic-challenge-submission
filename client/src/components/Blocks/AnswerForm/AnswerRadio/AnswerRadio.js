import "./AnswerRadio.css";
export function AnswerRadio({ nameKey, text, selectedAnswer, setSelectedAnswer, value }) {
  const handleClick = () => { setSelectedAnswer(value) }

  return (
    <div className="answers" onClick={handleClick}>
      <label>
        <input type="radio" name={nameKey} value={value} checked={selectedAnswer === value} readOnly />
          <span className="answer__radio">{ text }</span>
      </label>
    </div>
  )
}