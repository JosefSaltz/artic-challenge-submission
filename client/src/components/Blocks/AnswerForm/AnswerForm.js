import "./AnswerForm.css";
import { useState, useEffect } from "react";
import { AnswerRadio } from "./AnswerRadio/AnswerRadio"

export function AnswerForm({ blockData, nameKey, wasCorrect }) {
  // Set up selection state to pass to children
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [showFeedback, setShowFeedback] = useState(wasCorrect);
  // Destructure required data from blockData
  const { feedback, id } = blockData.block;
  const { answers, prevAnswers } = blockData;
  // Find the correct answer by it's boolean
  const correctAnswer = answers.find(answer => answer.isCorrect);
  // Determine if correct based on the correct answer's id
  const isCorrect = (answerID) => { return answerID === correctAnswer.id };
  // Check if there are previous answers and find a match if there is one
  const prevAnswer = prevAnswers.find(answer => answer.knowledgeCheckBlockId === id);
  // Submit handler that calls case specific handle functions
  const handleSubmit = (e) => {
    e.preventDefault(); 
    postAnswer(e);
  }
  // Submit post api handler
  const postAnswer = async () => {
    // Send POST to API
    const response = await fetch("http://localhost:5001/submit-answer", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        knowledgeCheckBlockId: id,
        answerId: selectedAnswer,
        wasCorrect: isCorrect(selectedAnswer)
      })
    })
    setShowFeedback(true);
    // Response guard
    if(!response.ok) { console.error("Something went wrong while POSTing answer") }
    // Update state for post answer submission
  };
  // Create an array of radio buttons with their appropriate props
  const answerChoices = answers.map((answer, index) => {
    // Assign props for readability
    const props = {
      checked: selectedAnswer === answer.id,
      key: index,
      nameKey,
      selectedAnswer,
      setSelectedAnswer,
      text: answer.text,
      value: answer.id
    }
    // Return our radio button with the spread props
    return (<AnswerRadio {...props} />)
  });
  // useEffect to automatically show the feedback and select the previous answer
  useEffect(() => {
    if(prevAnswer) {
      setSelectedAnswer(prevAnswer.answerId); 
      setShowFeedback(true);
    }
  }, [prevAnswer])

  return (
    <div className="answer__form">
      <form name={nameKey} onSubmit={handleSubmit} >
        { answerChoices }
        { 
          showFeedback 
            ? <div className="button__container">{feedback}</div>
            : <div className="button__container">
                <button type="submit" disabled={!selectedAnswer} onClick={handleSubmit}>Submit</button>
              </div> 
        }
      </form>
    </div>
  )
}