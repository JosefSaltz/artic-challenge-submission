import "./Block.css"
import { AnswerForm } from "./AnswerForm/AnswerForm.js"
import { BlockMedia } from "./BlockMedia/BlockMedia.js";

export function Block({ blockData, nameKey }) {
  const { mediaType, mediaURL, questionText } = blockData.block;
  return (
    <div className="block">
      <div className="block__content">
        { questionText }
        <BlockMedia mediaType={mediaType} mediaURL={mediaURL} />
        <AnswerForm blockData={blockData} nameKey={`block-form-${nameKey}`} />
      </div>
    </div>
  );
}