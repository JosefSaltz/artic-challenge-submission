import "./BlockList.css"
import { Block } from "../Block";
import { useBlockDataQuery } from "../../../hooks/queries/useBlockDataQuery"
// type BlocksListProps = { blockData: KnowledgeCheck[] }

export function BlocksList() {
  // Instantiate and assign query and it's return value
  const blockData = useBlockDataQuery();
  // Define our default content Loading
  let content = (<h1>Loading...</h1>);
  // Reassignment logic if successful or error
  if(blockData.isSuccess) content = (blockData.data.map((data, index) => <li key={index}><Block blockData={data} nameKey={index} /></li>));
  if(blockData.isError) {
    content = (<p>Sorry we're unable to show you content at this time</p>);
    console.error(blockData.error);
  };
  return (
    <div className="blocklist">
      { content }
    </div>
  )
}