import "../Block.css"

export function BlockMedia({ mediaType, mediaURL }) {
  
  return (
    <div>
      {mediaType === "image" && (<img src={mediaURL} alt="Unable to load" />)}
      {mediaType === "video" && (<video controls><source src={mediaURL} /></video>)}
    </div>
  );
}