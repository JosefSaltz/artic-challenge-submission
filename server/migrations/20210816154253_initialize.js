const { table } = require("../src/knex.js")

exports.up = async knex => {
  await Promise.all([
    knex.schema.createTable('knowledgeCheckBlocks', table => {
      table.uuid('id').primary()
      table.uuid('questionId')
      table.string('feedback')
    }),
    knex.schema.createTable('questions', table => {
      table.uuid('id').primary()
      table.string('text')
      table.uuid('mediaId')
    }),
    knex.schema.createTable('answers', table => {
      table.uuid('id').primary()
      table.uuid('knowledgeCheckBlockId')
      table.string('text')
      table.boolean('isCorrect')
      table.integer('pos')
    }),
    knex.schema.createTable('media', table => {
      table.uuid('id').primary()
      table.string('type')
      table.string('url')
    }),
    knex.schema.createTable('user_scores', table => {
      table.uuid('score_id').primary()
      table.uuid("knowledgeCheckBlockId")
      table.uuid('answerId')
      table.boolean("wasCorrect").nullable().defaultTo(null);
    })
  ])

  await Promise.all([
    knex.schema.table('knowledgeCheckBlocks', table => {
      table.foreign('questionId').references('questions.id')
    }),
    knex.schema.table('questions', table => {
      table.foreign('mediaId').references('media.id')
    }),
    knex.schema.table('answers', table => {
      table.foreign('knowledgeCheckBlockId').references('knowledgeCheckBlocks.id')
    }),
    knex.schema.table('user_scores', table => {
      table.foreign("knowledgeCheckBlockId").references('knowledgeCheckBlocks.id')
    }),
    knex.schema.table('user_scores', table => {
      table.foreign("answerId").references('answers.id')
    })
  ])
}

exports.down = async knex => {
  await Promise.all([
    knex.schema.table('knowledgeCheckBlocks', table => {
      table.dropForeign('questionId')
    }),
    knex.schema.table('questions', table => {
      table.dropForeign('mediaId')
    }),
    knex.schema.table('answers', table => {
      table.dropForeign('knowledgeCheckBlockId')
    }),
    knex.schema.table('user_scores', table => {
      table.dropForeign('knowledgeCheckBlockId')
    }),
    knex.schema.table('user_scores', table => {
      table.dropForeign('answerId')
    })
  ])

  await Promise.all([
    knex.schema.dropTable('knowledgeCheckBlocks'),
    knex.schema.dropTable('questions'),
    knex.schema.dropTable('answers'),
    knex.schema.dropTable('media'),
    knew.schema.dropTable('user_scores')
  ])
}
