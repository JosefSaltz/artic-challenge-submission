const express = require('express')
const morgan = require('morgan')
const cors = require("cors");
const knex = require('./knex')
const uuid = require("uuid");

const getKnowledgeCheckBlocks = async (req, res) =>{ 
  try {
    const blocks = await knex('knowledgeCheckBlocks')
      // Get the full knowledge blocks table for metadata purposes
      .select("knowledgeCheckBlocks.*")
      // Get the related question's text
      .join("questions", "knowledgeCheckBlocks.questionId", "=", "questions.id")
      .select("questions.text as questionText")
      // Get the media type and URL
      .join("media", "questions.mediaId", "=", "media.id")
      .select("media.type as mediaType", "media.url as mediaURL")
      //.join("user_scores", "user_scores.knowledgeCheckBlockId", "=", "knowledgeCheckBlocks.id")
    if(!blocks) throw new Error(`Something went wrong while getting knowledge blocks`)
      // Collect all of our relevant block IDs
    const blockIDs = blocks.map(block => block.id);
    // Find the answers that are associated
    const answers = await knex("answers")
      // With matching knowledgeCheckBlock IDs
      .whereIn("answers.knowledgeCheckBlockId", blockIDs);
    if(!answers) throw new Error(`Something went wrong while getting answers`);
    const prevAnswers = await knex("user_scores")
      .whereIn("user_scores.knowledgeCheckBlockId", blockIDs);
    // This is possibly a bit expensive for a production solution, but acts as a work around where
    // an ORM would be utilized
    // Build the response so that relevant answers are nested with their relevant block
    const response = blocks.map(block => ({ 
      block, 
      answers: answers.filter(answer => answer.knowledgeCheckBlockId === block.id), 
      prevAnswers: prevAnswers.filter(prev => prev.knowledgeCheckBlockId === block.id)
    }));
    // Respond with the strucutred response
    res.send(response)
  }
  catch(err) {
    console.error(err);
    res.send(500);
  }
}

const submitAnswerBlock = async (req, res) => {
  try {
    // Destructure the required data off the request body
    const { answerId, knowledgeCheckBlockId, wasCorrect } = req.body;
    // If were missing anything throw an error
    if(
      answerId === undefined || 
      wasCorrect === undefined || 
      knowledgeCheckBlockId === undefined
    ) throw new Error(`Parameters missing!`);
    // Insert the new record with the pulled data
    const result = await knex("user_scores").insert({ score_id: uuid.v4(), answerId, knowledgeCheckBlockId,wasCorrect });
    // Throw an error in case anything happened 
    if(!result) throw new Error(`Something went wrong while trying to insert user answer`);
    // Respond 200 OK
    res.send(200);
  }
  catch(err) {
    console.error(err);
    res.send(500); 
  }
}

function server() {
  const app = express();
  const port = 5001;
  // Not Production Safe
  const corsConfig = {
    origin: "*"
  };
  app.use(morgan('dev'));
  app.use(cors(corsConfig));
  app.use(express.json());
  app.get('/knowledge-check-blocks', getKnowledgeCheckBlocks);
  app.post('/submit-answer', submitAnswerBlock);
  app.start = app.listen.bind(app, port, () => console.log(`Listening on port ${port}`));

  return app
}

if (require.main === module) server().start()

module.exports = server